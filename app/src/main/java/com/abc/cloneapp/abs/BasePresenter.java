package com.abc.cloneapp.abs;

/**
 * @author Lody
 */
public interface BasePresenter {
	void start();
}
