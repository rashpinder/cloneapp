package com.abc.cloneapp.abs;

/**
 * @author Lody
 */

public interface Callback<T> {
    void callback(T result);
}
