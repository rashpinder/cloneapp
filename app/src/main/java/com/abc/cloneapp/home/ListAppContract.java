package com.abc.cloneapp.home;

import java.util.List;

import com.abc.cloneapp.abs.BasePresenter;
import com.abc.cloneapp.abs.BaseView;
import com.abc.cloneapp.home.models.AppInfo;

/**
 * @author Lody
 * @version 1.0
 */
/*package*/ class ListAppContract {
    interface ListAppView extends BaseView<ListAppPresenter> {

        void startLoading();

        void loadFinish(List<AppInfo> infoList);
    }

    interface ListAppPresenter extends BasePresenter {

    }
}
