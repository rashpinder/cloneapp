package com.abc.cloneapp.home;

import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.abc.cloneapp.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class SpeedModeActivity extends AppCompatActivity {
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    Switch switch_speed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speed_mode);
        add = findViewById(R.id.adView);
        loadAds();
        ImageView btn_back = (ImageView) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        Boolean speed_mode = prefs.getBoolean("speed_mode", false);
        switch_speed = (Switch) findViewById(R.id.switch_speed);
        switch_speed.setChecked(speed_mode);
        switch_speed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putBoolean("speed_mode", isChecked);
                editor.apply();
            }
        });

    }

    private AdView add;
    private InterstitialAd interstitialAd;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        assert add != null;
        add.destroy();
        displayInterstitial();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (add != null)
            add.pause();
        if (interstitialAd != null)
            interstitialAd.setAdListener(null);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (add != null)
            add.resume();
    }

    private void loadAds() {
        try {
            AdRequest adRequest = new AdRequest.Builder().build();
            add.loadAd(adRequest);
            interstitialAd = new InterstitialAd(SpeedModeActivity.this);
            interstitialAd.setAdUnitId(getString(R.string.admob_interstitial1));
            interstitialAd.loadAd(adRequest);
        } catch (
                Exception e) {
            Toast.makeText(SpeedModeActivity.this, e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    void displayInterstitial() {
        if (interstitialAd != null)
            interstitialAd.show();

    }
}
