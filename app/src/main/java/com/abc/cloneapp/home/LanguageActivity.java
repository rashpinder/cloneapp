package com.abc.cloneapp.home;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.abc.cloneapp.R;
import com.abc.cloneapp.home.adapters.LanguageAdapter;
import com.abc.cloneapp.home.models.Singleton;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class LanguageActivity extends AppCompatActivity {
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);
        add = findViewById(R.id.adView);
        loadAds();
        ImageView btn_back = (ImageView) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent languageIntent = new Intent(LanguageActivity.this, HomeActivity.class);
                LanguageActivity.this.startActivity(languageIntent);
                LanguageActivity.this.finish();
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LanguageAdapter languageAdapter = new LanguageAdapter(Singleton.getInstance().languageList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(languageAdapter);

    }

    @Override
    public void onBackPressed() {
        Intent languageIntent = new Intent(LanguageActivity.this, HomeActivity.class);
        LanguageActivity.this.startActivity(languageIntent);
        LanguageActivity.this.finish();
    }

    private AdView add;
    private InterstitialAd interstitialAd;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        assert add != null;
        add.destroy();
        displayInterstitial();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (add != null)
            add.pause();
        if (interstitialAd != null)
            interstitialAd.setAdListener(null);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (add != null)
            add.resume();
    }

    private void loadAds() {
        try {
            AdRequest adRequest = new AdRequest.Builder().build();
            add.loadAd(adRequest);
            interstitialAd = new InterstitialAd(LanguageActivity.this);
            interstitialAd.setAdUnitId(getString(R.string.admob_interstitial1));
            interstitialAd.loadAd(adRequest);
        } catch (
                Exception e) {
            Toast.makeText(LanguageActivity.this, e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    void displayInterstitial() {
        if (interstitialAd != null)
            interstitialAd.show();

    }
}
