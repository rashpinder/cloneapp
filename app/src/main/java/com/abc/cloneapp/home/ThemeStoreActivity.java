package com.abc.cloneapp.home;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.abc.cloneapp.R;
import com.abc.cloneapp.home.adapters.ThemeAdapter;
import com.abc.cloneapp.home.models.Singleton;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class ThemeStoreActivity extends AppCompatActivity {
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theme_store);
        add = findViewById(R.id.adView);
        loadAds();
        ImageView btn_back = (ImageView) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
//        recyclerView.setHasFixedSize(true);
//        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3, OrientationHelper.VERTICAL);
//        recyclerView.setLayoutManager(layoutManager);

        ThemeAdapter themeAdapter = new ThemeAdapter(Singleton.getInstance().themeList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(themeAdapter);


    }

    private AdView add;
    private InterstitialAd interstitialAd;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        assert add != null;
        add.destroy();
        displayInterstitial();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (add != null)
            add.pause();
        if (interstitialAd != null)
            interstitialAd.setAdListener(null);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (add != null)
            add.resume();
    }

    private void loadAds() {
        try {
            AdRequest adRequest = new AdRequest.Builder().build();
            add.loadAd(adRequest);
            interstitialAd = new InterstitialAd(ThemeStoreActivity.this);
            interstitialAd.setAdUnitId(getString(R.string.admob_interstitial2));
            interstitialAd.loadAd(adRequest);
        } catch (
                Exception e) {
            Toast.makeText(ThemeStoreActivity.this, e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    void displayInterstitial() {
        if (interstitialAd != null)
            interstitialAd.show();

    }
}
