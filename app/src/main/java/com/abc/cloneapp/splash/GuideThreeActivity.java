package com.abc.cloneapp.splash;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.abc.cloneapp.R;
import com.abc.cloneapp.home.HomeActivity;

public class GuideThreeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide_three);
        Button nextBtn = (Button) findViewById(R.id.btn_next);
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mainIntent = new Intent(GuideThreeActivity.this, HomeActivity.class);
                GuideThreeActivity.this.startActivity(mainIntent);
                GuideThreeActivity.this.finish();
            }
        });
    }
    @Override
    public void onBackPressed()
    {
        Intent prevIntent = new Intent(GuideThreeActivity.this, GuideTwoActivity.class);
        GuideThreeActivity.this.startActivity(prevIntent);
        GuideThreeActivity.this.finish();
    }
}
