package com.lody.virtual.client.stub;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.lody.virtual.R;

import static androidx.core.app.NotificationCompat.PRIORITY_MIN;


/**
 * @author Lody
 *
 */
public class DaemonService extends Service {

	private static final int NOTIFY_ID = 1001;

	public static void startup(Context context) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
		context.startForegroundService(new Intent(context, DaemonService.class));
	}}

	@Override
	public void onDestroy() {
		super.onDestroy();
		startup(this);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
			try {
				startForeground();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			}
		else
			startForeground(NOTIFY_ID, new Notification());
	}

	private void startForeground() {
		String channelId ="";
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			createNotificationChannel("my_service", "My Background Service");
		}else{
			NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(), channelId);
			Notification notification = notificationBuilder.setOngoing(true)
					.setPriority(PRIORITY_MIN)
					.setCategory(Notification.CATEGORY_SERVICE)
					.build();
			startForeground(NOTIFY_ID, notification);
		}
	}

	@RequiresApi(Build.VERSION_CODES.O)
	private String createNotificationChannel(String channelId ,String channelName){
		NotificationChannel chan = new NotificationChannel(channelId,
				channelName, NotificationManager.IMPORTANCE_NONE);
		chan.setLightColor(Color.BLUE);
		chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
		NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		manager.createNotificationChannel(chan);
		return channelId;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return START_STICKY;
	}

	public static final class InnerService extends Service {

		@Override
		public int onStartCommand(Intent intent, int flags, int startId) {
			startForeground(NOTIFY_ID, new Notification());
			stopForeground(true);
			stopSelf();
			return super.onStartCommand(intent, flags, startId);
		}

		@Override
		public IBinder onBind(Intent intent) {
			return null;
		}
	}


}
//
//public class DaemonService extends Service {
//
//	private static final int NOTIFY_ID = 1001;
//
//	public static void startup(Context context) {
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//			context.startService(new Intent(context, DaemonService.class));
//		}
//	}
//
//	@Override
//	public void onDestroy() {
//		super.onDestroy();
//		startup(this);
//
//		stopForeground(true);
//	}
//
//	@Override
//	public IBinder onBind(Intent intent) {
//		return null;
//	}
//
//	@Override
//	public void onCreate(){
//		super.onCreate();
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
//			startForeground();
//		else
//			startForeground(NOTIFY_ID, new Notification());
//	}
//	private void startForeground() {
//		String channelId ="";
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//			createNotificationChannel("my_service", "My Background Service");
//		}else{
//			NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(), channelId);
//			Notification notification = notificationBuilder.setOngoing(true)
//					.setPriority(PRIORITY_MIN)
//					.setCategory(Notification.CATEGORY_SERVICE)
//					.build();
//			startForeground(101, notification);
//		}
//	}
//
//	@RequiresApi(Build.VERSION_CODES.O)
//	private String createNotificationChannel(String channelId ,String channelName){
//		NotificationChannel chan = new NotificationChannel(channelId,
//				channelName, NotificationManager.IMPORTANCE_NONE);
//		chan.setLightColor(Color.BLUE);
//		chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
//		NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//		manager.createNotificationChannel(chan);
//		return channelId;
//	}
////	private void startMyOwnForeground(){
////		String NOTIFICATION_CHANNEL_ID = "com.example.simpleapp";
////		String channelName = "My Background Service";
////		NotificationChannel chan = null;
////		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
////			chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
////		}
////		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
////			chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
////		}
////		NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
////		assert manager != null;
////		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
////			manager.createNotificationChannel(chan);
////		}
////
////		NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
////		Notification notification = notificationBuilder.setOngoing(true)
////				.setSmallIcon(android.R.mipmap.sym_def_app_icon)
////				.setContentTitle("App is running in background")
////				.setPriority(NotificationManager.IMPORTANCE_MIN)
////				.setCategory(Notification.CATEGORY_SERVICE)
////				.build();
////		startForeground(NOTIFY_ID, notification);
////	}
//
////	@RequiresApi(Build.VERSION_CODES.O)
////	private String createNotificationChannel(String channelId, String channelName) {
////		NotificationChannel chan = new NotificationChannel(channelId,
////				channelName, NotificationManager.IMPORTANCE_NONE);
////		chan.setLightColor(Color.BLUE);
////		chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
////		NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
////		manager.createNotificationChannel(chan);
////		return channelId;
////	}
//
//	@Override
//	public int onStartCommand(Intent intent, int flags, int startId) {
//		return START_STICKY;
//	}
//
//	public static final class InnerService extends Service {
//
//		@Override
//		public int onStartCommand(Intent intent, int flags, int startId) {
//			startForeground(NOTIFY_ID, new Notification());
//			stopForeground(true);
//			stopSelf();
//			return super.onStartCommand(intent, flags, startId);
//		}
//
//		@Override
//		public IBinder onBind(Intent intent) {
//			return null;
//		}
//	}
//}